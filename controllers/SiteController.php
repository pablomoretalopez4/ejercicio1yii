<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select("edad")-> distinct(),
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['edad'],
           "titulo"=>"consulta 1 con Active Record",
           "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
           "sql"=>"SELECT DISTINCT edad FROM ciclista",
       ]);
    }
    public function actionConsulta1(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>'SELECT DISTINCT edad FROM ciclista',
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 1 con DAO",
           "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
           "sql"=>"SELECT DISTINCT edad FROM ciclista"
       ]);
       
    }
    
    
    //Consulta 2
    
    public function actionConsulta2a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select("edad")-> distinct() ->where("nomequipo='Artiach'"),
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['edad'],
           "titulo"=>"consulta 2 con Active Record",
           "enunciado"=>"Listar las edades de los ciclistas de Artiach",
           "sql"=>"SELECT DISTINCT edad from ciclista WHERE nomequipo='Artiach'",
       ]);
    }
    
     public function actionConsulta2(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>'SELECT DISTINCT edad from ciclista WHERE nomequipo="Artiach"',
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 2 con DAO",
           "enunciado"=>"Listar las edades de los ciclistas de Artiach",
           "sql"=>'SELECT DISTINCT edad from ciclista WHERE nomequipo="Artiach"',
       ]);
       
    }
    
    
    
    
    //Consulta 3
    
    
    public function actionConsulta3a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select("edad")-> distinct() ->where("nomequipo='Artiach' or nomequipo='Amore Vita'" ),
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['edad'],
           "titulo"=>"consulta 3 con Active Record",
           "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
           "sql"=>"SELECT DISTINCT edad from ciclista WHERE nomequipo ='Artiach' or nomequipo='Amore Vita'",
       ]);
       
       
       
    }
    public function actionConsulta3(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>"SELECT DISTINCT edad from ciclista WHERE nomequipo ='Artiach' or nomequipo='Amore Vita'",
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 3 con DAO",
           "enunciado"=>"Listar las edades de los ciclistas de Artiach",
           "sql"=>"SELECT DISTINCT edad from ciclista WHERE nomequipo ='Artiach' or nomequipo='Amore Vita'",
       ]);
    }
    
    
    
    
    
    //Consulta 4
    public function actionConsulta4a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select("dorsal")-> distinct() ->where("edad>25 and edad<30" ),
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 4 con Active Record",
           "enunciado"=>" Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
           "sql"=>"SELECT dorsal from ciclista WHERE edad>25 and edad<30",
       ]);
    }
    
    
     public function actionConsulta4(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>"SELECT dorsal from ciclista WHERE edad>25 and edad<30",
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 4 con DAO",
           "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
           "sql"=>"SELECT dorsal from ciclista WHERE edad>25 and edad<30",
       ]);
     }
     
     
     
     //Consulta 5
     
      public function actionConsulta5a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select("dorsal")-> distinct() ->where("(edad>28 and edad<32) and nomequipo='Banesto'"  ),
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 5 con Active Record",
           "enunciado"=>" Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
           "sql"=>"SELECT dorsal from ciclista WHERE (edad>28 and edad<32) and nomequipo='Banesto'",
       ]);
       
    }
    
    
    public function actionConsulta5(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>"SELECT dorsal from ciclista WHERE (edad>28 and edad<32) and nomequipo='Banesto'",
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 5 con DAO",
           "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
           "sql"=>"SELECT dorsal from ciclista WHERE (edad>28 and edad<32) and nomequipo='Banesto'",
       ]);
     }
     
     
     
     //Consulta 6
     
      public function actionConsulta6a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Ciclista::find()->select("nombre")-> distinct() ->where("CHAR_LENGTH(nombre)>8"  ),
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['nombre'],
           "titulo"=>"consulta 6 con Active Record",
           "enunciado"=>" Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
           "sql"=>"SELECT nombre FROM ciclista WHERE (CHAR_LENGTH(nombre)>8)",
       ]);
      }
      public function actionConsulta6(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>"SELECT nombre FROM ciclista WHERE (CHAR_LENGTH(nombre)>8)",
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre'],
           "titulo"=>"Consulta 6 con DAO",
           "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
           "sql"=>"SELECT nombre FROM ciclista WHERE (CHAR_LENGTH(nombre)>8)",
       ]);
     }
     
     
     
     
     //Consulta 7
   public function actionConsulta7a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
         'query' => \app\models\Ciclista::find()-> select("nombre, dorsal, UPPER(nombre) mayuscula")
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['nombre','dorsal', 'mayuscula'],
           "titulo"=>"consulta 7 con Active Record",
           "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
           "sql"=> "SELECT DISTINCT nombre, UPPER(nombre) as 'nombre mayúscula' FROM ciclista ",
       ]);
      }
      
       public function actionConsulta7(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>"SELECT nombre,dorsal, UPPER(nombre) as 'nombre_mayusculas' from ciclista",
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre', 'dorsal', 'nombre_mayusculas'],
           "titulo"=>"Consulta 7 con DAO",
           "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
           "sql"=>"SELECT nombre,dorsal, UPPER(nombre)  as 'nombre_mayusculas' from ciclista",
       ]);
     }
      
      
     
     
     
     
     
     
     
     //Consulta 8 
     
      public function actionConsulta8a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Lleva::find()->select("dorsal")-> distinct() ->where("código='MGE'" ),
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 8 con Active Record",
           "enunciado"=>" Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
           "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'",
       ]);
      }
         public function actionConsulta8(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'",
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 7 con DAO",
           "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
           "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'",
       ]);
     }
     
     
     
     
     //Consulta 9
     public function actionConsulta9a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Puerto::find()->select("nompuerto")-> distinct() ->where("altura>1500" ),
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['nompuerto'],
           "titulo"=>"consulta 9 con Active Record",
           "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500 ",
           "sql"=>"SELECT nompuerto FROM puerto WHERE altura>1500
",
       ]);
      }
         public function actionConsulta9(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>"SELECT nompuerto FROM puerto WHERE altura>1500
",
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['nompuerto'],
           "titulo"=>"Consulta 9 con DAO",
           "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500 ",
           "sql"=>"SELECT nompuerto FROM puerto WHERE altura>1500
",
       ]);
     }
     
     
     
     //Consulta 10
     public function actionConsulta10a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Puerto::find()->select("dorsal")-> distinct() ->where("(altura between 1800 and 3000) or (pendiente>8)" ),
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 10 con Active Record",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000 ",
           "sql"=>"SELECT DISTINCT dorsal from puerto where (altura BETWEEN 1800 and 3000) or (pendiente>8)",
       ]);
      }
      
      
       public function actionConsulta10(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>"SELECT DISTINCT dorsal from puerto where (altura BETWEEN 1800 and 3000) or (pendiente>8)",
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 10 con DAO",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
           "sql"=>"SELECT DISTINCT dorsal from puerto where (altura BETWEEN 1800 and 3000) or (pendiente>8)",
       ]);
     }
     
     
     
     
     //Consulta 11
      public function actionConsulta11a(){
        //Mediante Active record
        $dataProvider = new ActiveDataProvider([
        'query' => \app\models\Puerto::find()->select("dorsal")-> distinct() ->where("(altura between 1800 and 3000) and (pendiente>8)" ),
        ]);
        
       return $this->render("resultado",[
           "resultados"=> $dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"consulta 11 con Active Record",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000. ",
           "sql"=>"SELECT dorsal from puerto WHERE (pendiente>8) and (altura BETWEEN 1800 and 3000)",
       ]);
      }
      
      
      public function actionConsulta11(){
        //Mediante DAO
       $dataProvider=new SqlDataProvider([
           'sql'=>"SELECT dorsal from puerto WHERE (pendiente>8) and (altura BETWEEN 1800 and 3000)",
           
       ]);
       return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 11 con DAO",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000. ",
           "sql"=>"SELECT dorsal from puerto WHERE (pendiente>8) and (altura BETWEEN 1800 and 3000)",
       ]);
     }
    
      
      
   
     

}